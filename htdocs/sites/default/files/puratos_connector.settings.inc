<?php

define('BILLING_PROFILE_TYPE', 'RE');
define('SHIPPING_PROFILE_TYPE', 'WE');

/**
 * Contains all the settings of a Puratos webservice.
 *
 * @param string $webservice_name
 *
 * @return array $settings
 */
function puratos_connector_get_webservice_setting($webservice_name = '') {
  $settings = array();
  $environment = variable_get('environment');
  $multi_sales_orgs_param = puratos_connector_get_sales_org_soap_param();

  $languages = puratos_connector_get_soap_language_list();

  // Internal testing host: http://bizpur2010qpa.eur.puratos.grp:5080
  // Internal production host: http://bizpur2010prod.eur.puratos.grp:5080
  $host = 'http://bizpur2010qpa.eur.puratos.grp:5080';

  if ($environment == 'production') {
    $host = 'http://eai.puratos.com:5080/PROD';
  }

  // Settings of the customer web service.
  $settings['customers'] = array(
    'wsdl' => $host . '/esb/MyPuratos/Customer.svc?singleWsdl',
    'url' => $host . '/esb/MyPuratos/Customer.svc',
    // Active parameters.
    'parameters' => array(
      'SalesOrganizations' => $multi_sales_orgs_param,
      'CustomerNumber' => 'xxxxxxxxxx',
      'WebshopRelevant' => 'true',                         //TVCA1
    )
  );

  // Settings of the materials web service.
  $settings['materials'] = array(
    'wsdl' => $host . '/esb/MyPuratos/Material.svc?singleWsdl',
    'url' => $host . '/esb/MyPuratos/Material.svc',
    // Active parameters.
    // @todo OrgForWebshopRelevance should be a boolean.
    'parameters' => array(
      'SalesOrganizations' => $multi_sales_orgs_param,
      'WebshopRelevant' => 'true',
      'MaterialNumber' => 'xxxxxxxxxxxxxxxxxx',
      'Languages' => array(
        'Language' => $languages
      )
    ),
  );


  $settings['promo'] = array(
    'wsdl' => $host . '/esb/MyPuratos/Material.svc?singleWsdl',
    'url' => $host . '/esb/MyPuratos/Material.svc',
    // Active parameters.
    'parameters' => array(
      'SalesOrganizations' => $multi_sales_orgs_param,
      'MaterialType' => 'ZDIN',
      'MaterialNumber' => 'xxxxxxxxxxxxxxxxxx',
      'Languages' => $languages
    )
  );

  $settings['pricing_trigger'] = array(
    'wsdl' => $host . '/esb/MyPuratos/SalesOrder.svc?singleWsdl',
    'url' => $host . '/esb/MyPuratos/SalesOrder.svc',
    // Active parameters.
    'parameters' => array(
      'ConditionType' => 'ZPR0',
      'ValidOn' => puratos_connector_get_current_utc_timestamp(),
    )
  );

  // Settings of the pricing web service.
  $settings['pricing'] = array(
    'wsdl' => $host . '/esb/MyPuratos/SalesOrder.svc?singleWsdl',
    'url' => $host . '/esb/MyPuratos/SalesOrder.svc',
    // Active parameters.
    'parameters' => array(
      'TransactionId' => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
      'From' => 0,
      'To' => 100,
    )
  );

  // Settings of the order web service.
  $settings['sales_order'] = array(
    'wsdl' => $host . '/esb/MyPuratos/SalesOrder.svc?singleWsdl',
    'url' => $host . '/esb/MyPuratos/SalesOrder.svc',
    // Active parameters
    'parameters' => array(
      'CustomerNumber' => 'xxxxxxxxxx',
      'SalesOrganization' => '1101',
    ),
    'salesOffice' => '1101',
  );

  $settings['contracts'] = array(
    'wsdl' => $host . '/esb/MyPuratos/Contract.svc?singleWsdl',
    'url' => $host . '/esb/MyPuratos/Contract.svc',
    'parameters' => array(
      'SalesOrganizations' => $multi_sales_orgs_param,
      'ValidOn' => puratos_connector_get_current_utc_timestamp(),
      'WebshopRelevant' => 'true'
    ),
  );

  return empty($webservice_name) ? $settings : $settings[$webservice_name];
}
