About ACP Theme
====================
  Drupal standards compliant
  Implementation of a JS Slideshow
  Multi-level drop-down menus
  Footer with 4 regions
  A total of 10 regions
  Compatible and tested on IE7, IE8, IE9+, Opera, Firefox, Chrome browsers

Browser compatibility:
=====================
The theme has been tested on following browsers. IE7+, Firefox, Google Chrome, Opera.

Drupal compatibility:
=====================
This theme is compatible with Drupal 7.x.x

Developed by
============
Deploy-Service (c) 2016
CSS & Template based on Venture Theme by Devsaran

Help and Support Us
=====================
support@deploy-services.be