<?php

/**
 * @file
 * acp_registration.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function acp_registration_user_default_roles() {
  $roles = array();

  // Exported role: acp_admin.
  $roles['acp_admin'] = array(
    'name' => 'acp_admin',
    'weight' => 3,
  );

  // Exported role: acp_kampen.
  $roles['acp_kampen'] = array(
    'name' => 'acp_kampen',
    'weight' => 4,
  );

  return $roles;
}
