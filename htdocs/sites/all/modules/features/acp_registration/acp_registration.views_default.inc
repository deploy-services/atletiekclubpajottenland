<?php

/**
 * @file
 * acp_registration.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function acp_registration_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'acp_export_overpelt';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'acp_export_overpelt';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Export Overpelt';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    4 => '4',
    5 => '5',
    3 => '3',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Relationship: Registration: Node to Registration */
  $handler->display->display_options['relationships']['registration_rel']['id'] = 'registration_rel';
  $handler->display->display_options['relationships']['registration_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['registration_rel']['field'] = 'registration_rel';
  $handler->display->display_options['relationships']['registration_rel']['required'] = TRUE;
  /* Field: Registration: Registration ID */
  $handler->display->display_options['fields']['registration_id']['id'] = 'registration_id';
  $handler->display->display_options['fields']['registration_id']['table'] = 'registration';
  $handler->display->display_options['fields']['registration_id']['field'] = 'registration_id';
  $handler->display->display_options['fields']['registration_id']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['registration_id']['label'] = 'Inschrijvig ID';
  /* Field: Registration: Anonymous e-mail */
  $handler->display->display_options['fields']['anon_mail']['id'] = 'anon_mail';
  $handler->display->display_options['fields']['anon_mail']['table'] = 'registration';
  $handler->display->display_options['fields']['anon_mail']['field'] = 'anon_mail';
  $handler->display->display_options['fields']['anon_mail']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['anon_mail']['label'] = 'E-mail';
  /* Field: Registration: Email ouder 2 (optioneel) */
  $handler->display->display_options['fields']['field_registrant_email_2']['id'] = 'field_registrant_email_2';
  $handler->display->display_options['fields']['field_registrant_email_2']['table'] = 'field_data_field_registrant_email_2';
  $handler->display->display_options['fields']['field_registrant_email_2']['field'] = 'field_registrant_email_2';
  $handler->display->display_options['fields']['field_registrant_email_2']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['field_registrant_email_2']['label'] = 'E-mail 2';
  /* Field: Registration: Rendered Registration */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_registration';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['rendered_entity']['label'] = 'Groepsnaam';
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'full';
  $handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
  /* Field: Registration: State entity */
  $handler->display->display_options['fields']['state']['id'] = 'state';
  $handler->display->display_options['fields']['state']['table'] = 'registration';
  $handler->display->display_options['fields']['state']['field'] = 'state';
  $handler->display->display_options['fields']['state']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['state']['label'] = 'Status';
  /* Field: Registration: Date created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'registration';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['created']['label'] = 'Inschrijvingsdatum';
  $handler->display->display_options['fields']['created']['date_format'] = 'short';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'search_api_facetapi_YEAR';
  /* Field: Registration: Adres- en persoonsgegevens van uw kind - First name */
  $handler->display->display_options['fields']['field_registrant_address_first_name']['id'] = 'field_registrant_address_first_name';
  $handler->display->display_options['fields']['field_registrant_address_first_name']['table'] = 'field_data_field_registrant_address';
  $handler->display->display_options['fields']['field_registrant_address_first_name']['field'] = 'field_registrant_address_first_name';
  $handler->display->display_options['fields']['field_registrant_address_first_name']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['field_registrant_address_first_name']['label'] = 'Voornaam';
  /* Field: Registration: Adres- en persoonsgegevens van uw kind - Last name */
  $handler->display->display_options['fields']['field_registrant_address_last_name']['id'] = 'field_registrant_address_last_name';
  $handler->display->display_options['fields']['field_registrant_address_last_name']['table'] = 'field_data_field_registrant_address';
  $handler->display->display_options['fields']['field_registrant_address_last_name']['field'] = 'field_registrant_address_last_name';
  $handler->display->display_options['fields']['field_registrant_address_last_name']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['field_registrant_address_last_name']['label'] = 'Achternaam';
  /* Field: Registration: Geboortedatum van uw kind */
  $handler->display->display_options['fields']['field_registrant_dob']['id'] = 'field_registrant_dob';
  $handler->display->display_options['fields']['field_registrant_dob']['table'] = 'field_data_field_registrant_dob';
  $handler->display->display_options['fields']['field_registrant_dob']['field'] = 'field_registrant_dob';
  $handler->display->display_options['fields']['field_registrant_dob']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['field_registrant_dob']['label'] = 'Geboortedatum';
  $handler->display->display_options['fields']['field_registrant_dob']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Registration: Adres- en persoonsgegevens van uw kind - Thoroughfare (i.e. Street address) */
  $handler->display->display_options['fields']['field_registrant_address_thoroughfare']['id'] = 'field_registrant_address_thoroughfare';
  $handler->display->display_options['fields']['field_registrant_address_thoroughfare']['table'] = 'field_data_field_registrant_address';
  $handler->display->display_options['fields']['field_registrant_address_thoroughfare']['field'] = 'field_registrant_address_thoroughfare';
  $handler->display->display_options['fields']['field_registrant_address_thoroughfare']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['field_registrant_address_thoroughfare']['label'] = 'Straat+Nummer';
  /* Field: Registration: Adres- en persoonsgegevens van uw kind - Premise (i.e. Apartment / Suite number) */
  $handler->display->display_options['fields']['field_registrant_address_premise']['id'] = 'field_registrant_address_premise';
  $handler->display->display_options['fields']['field_registrant_address_premise']['table'] = 'field_data_field_registrant_address';
  $handler->display->display_options['fields']['field_registrant_address_premise']['field'] = 'field_registrant_address_premise';
  $handler->display->display_options['fields']['field_registrant_address_premise']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['field_registrant_address_premise']['label'] = 'Toevoeging';
  /* Field: Registration: Adres- en persoonsgegevens van uw kind - Postal code */
  $handler->display->display_options['fields']['field_registrant_address_postal_code']['id'] = 'field_registrant_address_postal_code';
  $handler->display->display_options['fields']['field_registrant_address_postal_code']['table'] = 'field_data_field_registrant_address';
  $handler->display->display_options['fields']['field_registrant_address_postal_code']['field'] = 'field_registrant_address_postal_code';
  $handler->display->display_options['fields']['field_registrant_address_postal_code']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['field_registrant_address_postal_code']['label'] = 'Postcode';
  /* Field: Registration: Adres- en persoonsgegevens van uw kind - Locality (i.e. City) */
  $handler->display->display_options['fields']['field_registrant_address_locality']['id'] = 'field_registrant_address_locality';
  $handler->display->display_options['fields']['field_registrant_address_locality']['table'] = 'field_data_field_registrant_address';
  $handler->display->display_options['fields']['field_registrant_address_locality']['field'] = 'field_registrant_address_locality';
  $handler->display->display_options['fields']['field_registrant_address_locality']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['field_registrant_address_locality']['label'] = 'Plaats';
  /* Field: Registration: Telefoonnummer ouder 1 */
  $handler->display->display_options['fields']['field_registrant_tel_1']['id'] = 'field_registrant_tel_1';
  $handler->display->display_options['fields']['field_registrant_tel_1']['table'] = 'field_data_field_registrant_tel_1';
  $handler->display->display_options['fields']['field_registrant_tel_1']['field'] = 'field_registrant_tel_1';
  $handler->display->display_options['fields']['field_registrant_tel_1']['relationship'] = 'registration_rel';
  /* Field: Registration: Telefoonnummer ouder 2/andere contactpersoon */
  $handler->display->display_options['fields']['field_registrant_tel_2']['id'] = 'field_registrant_tel_2';
  $handler->display->display_options['fields']['field_registrant_tel_2']['table'] = 'field_data_field_registrant_tel_2';
  $handler->display->display_options['fields']['field_registrant_tel_2']['field'] = 'field_registrant_tel_2';
  $handler->display->display_options['fields']['field_registrant_tel_2']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['field_registrant_tel_2']['label'] = 'Telefoonnummer ouder 2';
  /* Field: Registration: Nam reeds deel aan volgende wedstrijden */
  $handler->display->display_options['fields']['field_registrant_tx_wedstrijden']['id'] = 'field_registrant_tx_wedstrijden';
  $handler->display->display_options['fields']['field_registrant_tx_wedstrijden']['table'] = 'field_data_field_registrant_tx_wedstrijden';
  $handler->display->display_options['fields']['field_registrant_tx_wedstrijden']['field'] = 'field_registrant_tx_wedstrijden';
  $handler->display->display_options['fields']['field_registrant_tx_wedstrijden']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['field_registrant_tx_wedstrijden']['label'] = 'Nam reeds deel aan';
  $handler->display->display_options['fields']['field_registrant_tx_wedstrijden']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_registrant_tx_wedstrijden']['delta_offset'] = '0';
  /* Filter criterion: Content: Sportkamp Locatie (field_sportkamp_locatie) */
  $handler->display->display_options['filters']['field_sportkamp_locatie_tid']['id'] = 'field_sportkamp_locatie_tid';
  $handler->display->display_options['filters']['field_sportkamp_locatie_tid']['table'] = 'field_data_field_sportkamp_locatie';
  $handler->display->display_options['filters']['field_sportkamp_locatie_tid']['field'] = 'field_sportkamp_locatie_tid';
  $handler->display->display_options['filters']['field_sportkamp_locatie_tid']['value'] = array(
    62 => '62',
  );
  $handler->display->display_options['filters']['field_sportkamp_locatie_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_sportkamp_locatie_tid']['vocabulary'] = 'locaties_kampen';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'acp-export-overpelt';

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['style_plugin'] = 'views_data_export_xls';
  $handler->display->display_options['style_options']['provide_file'] = 1;
  $handler->display->display_options['style_options']['parent_sort'] = 0;
  $handler->display->display_options['path'] = 'export-overpelt-xls';
  $translatables['acp_export_overpelt'] = array(
    t('Master'),
    t('Export Overpelt'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('node being the Registration'),
    t('Inschrijvig ID'),
    t('.'),
    t(','),
    t('E-mail'),
    t('E-mail 2'),
    t('Groepsnaam'),
    t('Status'),
    t('Inschrijvingsdatum'),
    t('Voornaam'),
    t('Achternaam'),
    t('Geboortedatum'),
    t('Straat+Nummer'),
    t('Toevoeging'),
    t('Postcode'),
    t('Plaats'),
    t('Telefoonnummer ouder 1'),
    t('Telefoonnummer ouder 2'),
    t('Nam reeds deel aan'),
    t('Page'),
    t('Data export'),
  );
  $export['acp_export_overpelt'] = $view;

  $view = new view();
  $view->name = 'acp_registrations';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'acp_registrations';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Overzicht Inschrijvingen';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    4 => '4',
    5 => '5',
    3 => '3',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Relationship: Registration: Node to Registration */
  $handler->display->display_options['relationships']['registration_rel']['id'] = 'registration_rel';
  $handler->display->display_options['relationships']['registration_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['registration_rel']['field'] = 'registration_rel';
  $handler->display->display_options['relationships']['registration_rel']['required'] = TRUE;
  /* Field: Registration: Registration ID */
  $handler->display->display_options['fields']['registration_id']['id'] = 'registration_id';
  $handler->display->display_options['fields']['registration_id']['table'] = 'registration';
  $handler->display->display_options['fields']['registration_id']['field'] = 'registration_id';
  $handler->display->display_options['fields']['registration_id']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['registration_id']['exclude'] = TRUE;
  /* Field: Registration: Rendered Registration */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_registration';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['rendered_entity']['label'] = 'Groepsnaam';
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'full';
  $handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
  /* Field: Registration: Anonymous e-mail */
  $handler->display->display_options['fields']['anon_mail']['id'] = 'anon_mail';
  $handler->display->display_options['fields']['anon_mail']['table'] = 'registration';
  $handler->display->display_options['fields']['anon_mail']['field'] = 'anon_mail';
  $handler->display->display_options['fields']['anon_mail']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['anon_mail']['label'] = 'E-mail';
  /* Field: Registration: Adres- en persoonsgegevens van uw kind - First name */
  $handler->display->display_options['fields']['field_registrant_address_first_name']['id'] = 'field_registrant_address_first_name';
  $handler->display->display_options['fields']['field_registrant_address_first_name']['table'] = 'field_data_field_registrant_address';
  $handler->display->display_options['fields']['field_registrant_address_first_name']['field'] = 'field_registrant_address_first_name';
  $handler->display->display_options['fields']['field_registrant_address_first_name']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['field_registrant_address_first_name']['label'] = 'Voornaam';
  /* Field: Registration: Adres- en persoonsgegevens van uw kind - Last name */
  $handler->display->display_options['fields']['field_registrant_address_last_name']['id'] = 'field_registrant_address_last_name';
  $handler->display->display_options['fields']['field_registrant_address_last_name']['table'] = 'field_data_field_registrant_address';
  $handler->display->display_options['fields']['field_registrant_address_last_name']['field'] = 'field_registrant_address_last_name';
  $handler->display->display_options['fields']['field_registrant_address_last_name']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['field_registrant_address_last_name']['label'] = 'Achternaam';
  /* Field: Registration: Geboortedatum van uw kind */
  $handler->display->display_options['fields']['field_registrant_dob']['id'] = 'field_registrant_dob';
  $handler->display->display_options['fields']['field_registrant_dob']['table'] = 'field_data_field_registrant_dob';
  $handler->display->display_options['fields']['field_registrant_dob']['field'] = 'field_registrant_dob';
  $handler->display->display_options['fields']['field_registrant_dob']['relationship'] = 'registration_rel';
  $handler->display->display_options['fields']['field_registrant_dob']['label'] = 'Geboortedatum';
  $handler->display->display_options['fields']['field_registrant_dob']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: Sportkamp Locatie */
  $handler->display->display_options['fields']['field_sportkamp_locatie']['id'] = 'field_sportkamp_locatie';
  $handler->display->display_options['fields']['field_sportkamp_locatie']['table'] = 'field_data_field_sportkamp_locatie';
  $handler->display->display_options['fields']['field_sportkamp_locatie']['field'] = 'field_sportkamp_locatie';
  $handler->display->display_options['fields']['field_sportkamp_locatie']['label'] = 'Locatie';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'acp-registrations';
  $translatables['acp_registrations'] = array(
    t('Master'),
    t('Overzicht Inschrijvingen'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('node being the Registration'),
    t('Registration ID'),
    t('.'),
    t(','),
    t('Groepsnaam'),
    t('E-mail'),
    t('Voornaam'),
    t('Achternaam'),
    t('Geboortedatum'),
    t('Locatie'),
    t('Page'),
  );
  $export['acp_registrations'] = $view;

  $view = new view();
  $view->name = 'acp_sportkampen';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'acp_sportkampen';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Inschrijven voor sportkampen';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'Momenteel zijn er geen sportkampen beschikbaar';
  /* Relationship: Registration Settings: Node to Registration Settings */
  $handler->display->display_options['relationships']['registration_settings_rel']['id'] = 'registration_settings_rel';
  $handler->display->display_options['relationships']['registration_settings_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['registration_settings_rel']['field'] = 'registration_settings_rel';
  $handler->display->display_options['relationships']['registration_settings_rel']['required'] = TRUE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = 'Node ID';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  /* Field: Registration Settings: Spaces used */
  $handler->display->display_options['fields']['capacity_used']['id'] = 'capacity_used';
  $handler->display->display_options['fields']['capacity_used']['table'] = 'node';
  $handler->display->display_options['fields']['capacity_used']['field'] = 'capacity_used';
  $handler->display->display_options['fields']['capacity_used']['exclude'] = TRUE;
  /* Field: Registration Settings: Open Date */
  $handler->display->display_options['fields']['open']['id'] = 'open';
  $handler->display->display_options['fields']['open']['table'] = 'registration_entity';
  $handler->display->display_options['fields']['open']['field'] = 'open';
  $handler->display->display_options['fields']['open']['relationship'] = 'registration_settings_rel';
  $handler->display->display_options['fields']['open']['exclude'] = TRUE;
  $handler->display->display_options['fields']['open']['date_format'] = 'short';
  $handler->display->display_options['fields']['open']['second_date_format'] = 'search_api_facetapi_YEAR';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Groep';
  $handler->display->display_options['fields']['title']['alter']['path'] = '/node/[nid]/register';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Sportkamp Locatie */
  $handler->display->display_options['fields']['field_sportkamp_locatie']['id'] = 'field_sportkamp_locatie';
  $handler->display->display_options['fields']['field_sportkamp_locatie']['table'] = 'field_data_field_sportkamp_locatie';
  $handler->display->display_options['fields']['field_sportkamp_locatie']['field'] = 'field_sportkamp_locatie';
  $handler->display->display_options['fields']['field_sportkamp_locatie']['label'] = 'Locatie';
  $handler->display->display_options['fields']['field_sportkamp_locatie']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Sportkamp Data */
  $handler->display->display_options['fields']['field_sportkamp_data']['id'] = 'field_sportkamp_data';
  $handler->display->display_options['fields']['field_sportkamp_data']['table'] = 'field_data_field_sportkamp_data';
  $handler->display->display_options['fields']['field_sportkamp_data']['field'] = 'field_sportkamp_data';
  $handler->display->display_options['fields']['field_sportkamp_data']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Registration Settings: Slots Total */
  $handler->display->display_options['fields']['capacity_total']['id'] = 'capacity_total';
  $handler->display->display_options['fields']['capacity_total']['table'] = 'registration_entity';
  $handler->display->display_options['fields']['capacity_total']['field'] = 'capacity_total';
  $handler->display->display_options['fields']['capacity_total']['relationship'] = 'registration_settings_rel';
  $handler->display->display_options['fields']['capacity_total']['label'] = 'Ingenomen plaatsen';
  $handler->display->display_options['fields']['capacity_total']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['capacity_total']['alter']['text'] = '[capacity_used] / [capacity_total]';
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php']['id'] = 'php';
  $handler->display->display_options['fields']['php']['table'] = 'views';
  $handler->display->display_options['fields']['php']['field'] = 'php';
  $handler->display->display_options['fields']['php']['label'] = 'Inschrijven';
  $handler->display->display_options['fields']['php']['empty'] = '<a href="node/[nid]/register">Schrijf Deelnemer In</a>';
  $handler->display->display_options['fields']['php']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php']['php_value'] = '$now = date("Y-m-d h:i:s");
if ( $now < $row->open ){
$date = date_create($row->open);
$open = date_format($date, "d-m-Y");
return "vanaf ". $open;
}
';
  $handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable'] = '';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'sportkamp' => 'sportkamp',
  );
  /* Filter criterion: Content: Sportkamp Data - end date (field_sportkamp_data:value2) */
  $handler->display->display_options['filters']['field_sportkamp_data_value2']['id'] = 'field_sportkamp_data_value2';
  $handler->display->display_options['filters']['field_sportkamp_data_value2']['table'] = 'field_data_field_sportkamp_data';
  $handler->display->display_options['filters']['field_sportkamp_data_value2']['field'] = 'field_sportkamp_data_value2';
  $handler->display->display_options['filters']['field_sportkamp_data_value2']['operator'] = '>';
  $handler->display->display_options['filters']['field_sportkamp_data_value2']['default_date'] = 'now';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'acp-sportkampen';
  $translatables['acp_sportkampen'] = array(
    t('Master'),
    t('Inschrijven voor sportkampen'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Momenteel zijn er geen sportkampen beschikbaar'),
    t('node being the Registration'),
    t('Node ID'),
    t('Spaces used'),
    t('.'),
    t(','),
    t('Open Date'),
    t('Groep'),
    t('Locatie'),
    t('Sportkamp Data'),
    t('Ingenomen plaatsen'),
    t('[capacity_used] / [capacity_total]'),
    t('Inschrijven'),
    t('<a href="node/[nid]/register">Schrijf Deelnemer In</a>'),
    t('Page'),
  );
  $export['acp_sportkampen'] = $view;

  return $export;
}
