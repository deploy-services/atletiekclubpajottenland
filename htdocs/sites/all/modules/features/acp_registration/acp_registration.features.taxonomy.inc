<?php

/**
 * @file
 * acp_registration.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function acp_registration_taxonomy_default_vocabularies() {
  return array(
    'locaties_kampen' => array(
      'name' => 'Locaties Kampen',
      'machine_name' => 'locaties_kampen',
      'description' => 'Locaties waar de sportkampen/zomerkampen doorgaan',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'wedstrijden_registratieformulier' => array(
      'name' => 'Wedstrijden registratieformulier',
      'machine_name' => 'wedstrijden_registratieformulier',
      'description' => 'Reeds deelgenomen wedstrijden op het registratieformulier',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
