<?php

/**
 * @file
 * acp_registration.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function acp_registration_default_rules_configuration() {
  $items = array();
  $items['rules_event_registration_confirmation_email'] = entity_import('rules_config', '{ "rules_event_registration_confirmation_email" : {
      "LABEL" : "Event Registration Confirmation Email",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "registration" ],
      "ON" : { "registration_insert" : [] },
      "DO" : [
        { "mail" : {
            "to" : "[registration:anon-mail]",
            "subject" : "Inschrijving voor sportkamp [registration:entity]",
            "message" : "Beste,\\r\\n\\u003Cbr\\u003E\\r\\n\\u003Cp\\u003E\\r\\nBij deze bevestigen wij de inschrijving van uw zoon\\/dochter voor het sportkamp te [registration:entity:field-sportkamp-locatie]. Gelieve de onderstaande gegevens nogmaals te controleren. Fouten in de informatie mag u melden via acpjeugd@skynet.be\\r\\n\\u003C\\/p\\u003E\\r\\n\\u003Cp\\u003E\\u003Cb\\u003E\\u003Ci\\u003E\\r\\nGelieve geen 2e keer in te schrijven, om dubbele boekingen te vermijden.\\r\\n\\u003C\\/i\\u003E\\u003C\\/b\\u003E\\u003C\\/p\\u003E\\r\\n\\u003Cp\\u003E\\r\\nNaam van het kind: [registration:field-registrant-address:name_line]\\u003Cbr\\u003E\\r\\nGeboortedatum: [registration:field-registrant-dob]\\r\\n\\u003C\\/p\\u003E\\u003Cp\\u003E\\r\\nAdres: \\u003Cbr\\u003E[registration:field-registrant-address:thoroughfare] [registration:field-registrant-address:premise]\\u003Cbr\\u003E\\r\\n[registration:field-registrant-address:postal_code] [registration:field-registrant-address:locality]\\r\\n\\u003C\\/p\\u003E\\r\\n\\u003Cp\\u003E\\r\\nTelefoonnummer(s) van de\\/beide ouder(s) in noodgevallen:\\u003Cbr\\u003E\\r\\nOuder 1: [registration:field-registrant-tel-1]\\u003Cbr\\u003E\\r\\nOuder 2: [registration:field-registrant-tel-2]\\u003Cbr\\u003E\\r\\n\\u003C\\/p\\u003E\\r\\n\\u003Cp\\u003E\\r\\nHet kampboek met praktische informatie, betalingsinfo en de medische fiche kan u downloaden via www.atletiekclubpajottenland.be \\u003E kampen en stages \\u003E documenten of via deze link: http:\\/\\/atletiekclubpajottenland.be\\/node\\/191. \\r\\n\\u003C\\/p\\u003E\\r\\nWij zijn er klaar voor, jullie ook!?",
            "from" : "sportkampen@atletiekclubpajottenland.be",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
