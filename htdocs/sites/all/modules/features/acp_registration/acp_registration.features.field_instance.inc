<?php

/**
 * @file
 * acp_registration.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function acp_registration_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-sportkamp-field_registratie'.
  $field_instances['node-sportkamp-field_registratie'] = array(
    'bundle' => 'sportkamp',
    'default_value' => array(
      0 => array(
        'registration_type' => 'sportkamp',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'registration',
        'settings' => array(
          'i18n_string_key' => NULL,
          'label' => NULL,
        ),
        'type' => 'registration_link',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_registratie',
    'label' => 'Registratie',
    'required' => 0,
    'settings' => array(
      'default_registration_settings' => array(
        'capacity' => 0,
        'reminder' => array(
          'reminder_settings' => array(
            'reminder_date' => NULL,
            'reminder_template' => '',
          ),
          'send_reminder' => 0,
        ),
        'scheduling' => array(
          'close' => NULL,
          'open' => NULL,
        ),
        'settings' => array(
          'confirmation' => 'Registration has been saved.',
          'confirmation_redirect' => '',
          'from_address' => 'noreply@atletiekclubpajottenland.be',
          'maximum_spaces' => 1,
          'multiple_registrations' => 0,
        ),
        'status' => 1,
      ),
      'hide_register_tab' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'registration',
      'settings' => array(),
      'type' => 'registration_select',
      'weight' => -3,
    ),
  );

  // Exported field_instance: 'node-sportkamp-field_sportkamp_data'.
  $field_instances['node-sportkamp-field_sportkamp_data'] = array(
    'bundle' => 'sportkamp',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => FALSE,
        ),
        'type' => 'date_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_sportkamp_data',
    'label' => 'Sportkamp Data',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'no_fieldset' => 0,
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => -4,
    ),
  );

  // Exported field_instance: 'node-sportkamp-field_sportkamp_locatie'.
  $field_instances['node-sportkamp-field_sportkamp_locatie'] = array(
    'bundle' => 'sportkamp',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_sportkamp_locatie',
    'label' => 'Sportkamp Locatie',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => -2,
    ),
  );

  // Exported field_instance: 'node-sportkamp-field_sportkamp_yob_from'.
  $field_instances['node-sportkamp-field_sportkamp_yob_from'] = array(
    'bundle' => 'sportkamp',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => FALSE,
        ),
        'type' => 'date_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_sportkamp_yob_from',
    'label' => 'Geboortejaar Van',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'd/m/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'no_fieldset' => 0,
        'text_parts' => array(),
        'year_range' => '-20:-4',
      ),
      'type' => 'date_select',
      'weight' => -1,
    ),
  );

  // Exported field_instance: 'node-sportkamp-field_sportkamp_yob_to'.
  $field_instances['node-sportkamp-field_sportkamp_yob_to'] = array(
    'bundle' => 'sportkamp',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => FALSE,
        ),
        'type' => 'date_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_sportkamp_yob_to',
    'label' => 'Geboortejaar Tot',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'd/m/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'no_fieldset' => 0,
        'text_parts' => array(),
        'year_range' => '-20:-4',
      ),
      'type' => 'date_select',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'registration-sportkamp-field_registrant_address'.
  $field_instances['registration-sportkamp-field_registrant_address'] = array(
    'bundle' => 'sportkamp',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'addressfield',
        'settings' => array(
          'format_handlers' => array(
            0 => 'address',
          ),
          'use_widget_handlers' => 1,
        ),
        'type' => 'addressfield_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_registrant_address',
    'label' => 'Adres- en persoonsgegevens van uw kind',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'addressfield',
      'settings' => array(
        'available_countries' => array(
          'BE' => 'BE',
        ),
        'default_country' => 'site_default',
        'format_handlers' => array(
          'address' => 'address',
          'address-hide-country' => 'address-hide-country',
          'name-full' => 'name-full',
          'address-hide-postal-code' => 0,
          'address-hide-street' => 0,
          'organisation' => 0,
          'name-oneline' => 0,
          'address-optional' => 0,
        ),
      ),
      'type' => 'addressfield_standard',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'registration-sportkamp-field_registrant_dob'.
  $field_instances['registration-sportkamp-field_registrant_dob'] = array(
    'bundle' => 'sportkamp',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => FALSE,
        ),
        'type' => 'date_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_registrant_dob',
    'label' => 'Geboortedatum van uw kind',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'd/m/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'no_fieldset' => 0,
        'text_parts' => array(),
        'year_range' => '-20:-4',
      ),
      'type' => 'date_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'registration-sportkamp-field_registrant_email_2'.
  $field_instances['registration-sportkamp-field_registrant_email_2'] = array(
    'bundle' => 'sportkamp',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Additioneel adres waarnaar alle communicatie ivm het kamp zal gestuurd worden.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'email',
        'settings' => array(),
        'type' => 'email_default',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_registrant_email_2',
    'label' => 'Email ouder 2 (optioneel)',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'email',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'email_textfield',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'registration-sportkamp-field_registrant_tel_1'.
  $field_instances['registration-sportkamp-field_registrant_tel_1'] = array(
    'bundle' => 'sportkamp',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_plain',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_registrant_tel_1',
    'label' => 'Telefoonnummer ouder 1',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'telephone',
      'settings' => array(
        'placeholder' => '',
      ),
      'type' => 'telephone_default',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'registration-sportkamp-field_registrant_tel_2'.
  $field_instances['registration-sportkamp-field_registrant_tel_2'] = array(
    'bundle' => 'sportkamp',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Een tweede contactpersoon is verplicht op te geven, indien het telefoonnummer \'ouder 1\' bij een noodgeval onbereikbaar blijkt.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_plain',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_registrant_tel_2',
    'label' => 'Telefoonnummer ouder 2/andere contactpersoon',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'telephone',
      'settings' => array(
        'placeholder' => '',
      ),
      'type' => 'telephone_default',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'registration-sportkamp-field_registrant_tx_wedstrijden'.
  $field_instances['registration-sportkamp-field_registrant_tx_wedstrijden'] = array(
    'bundle' => 'sportkamp',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_registrant_tx_wedstrijden',
    'label' => 'Nam reeds deel aan volgende wedstrijden',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 5,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Additioneel adres waarnaar alle communicatie ivm het kamp zal gestuurd worden.');
  t('Adres- en persoonsgegevens van uw kind');
  t('Een tweede contactpersoon is verplicht op te geven, indien het telefoonnummer \'ouder 1\' bij een noodgeval onbereikbaar blijkt.');
  t('Email ouder 2 (optioneel)');
  t('Geboortedatum van uw kind');
  t('Geboortejaar Tot');
  t('Geboortejaar Van');
  t('Nam reeds deel aan volgende wedstrijden');
  t('Registratie');
  t('Sportkamp Data');
  t('Sportkamp Locatie');
  t('Telefoonnummer ouder 1');
  t('Telefoonnummer ouder 2/andere contactpersoon');

  return $field_instances;
}
