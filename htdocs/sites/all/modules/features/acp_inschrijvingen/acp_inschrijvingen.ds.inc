<?php

/**
 * @file
 * acp_inschrijvingen.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function acp_inschrijvingen_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'commerce_customer_profile|billing|default';
  $ds_layout->entity_type = 'commerce_customer_profile';
  $ds_layout->bundle = 'billing';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_3col_equal_width';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'commerce_customer_address',
        1 => 'field_inschrijving_tel',
      ),
      'middle' => array(
        2 => 'field_inschrijving_dob',
        3 => 'field_inschrijving_geslacht',
      ),
      'right' => array(
        4 => 'field_training_locatie',
        5 => 'field_vrijwilliger',
      ),
    ),
    'fields' => array(
      'commerce_customer_address' => 'left',
      'field_inschrijving_tel' => 'left',
      'field_inschrijving_dob' => 'middle',
      'field_inschrijving_geslacht' => 'middle',
      'field_training_locatie' => 'right',
      'field_vrijwilliger' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'middle' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['commerce_customer_profile|billing|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'commerce_order|commerce_order|default';
  $ds_layout->entity_type = 'commerce_order';
  $ds_layout->bundle = 'commerce_order';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col_wrapper';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'commerce_line_items',
        1 => 'commerce_order_total',
        2 => 'commerce_customer_billing',
        3 => 'field_mededeling',
        4 => 'field_acp_order_status',
      ),
    ),
    'fields' => array(
      'commerce_line_items' => 'ds_content',
      'commerce_order_total' => 'ds_content',
      'commerce_customer_billing' => 'ds_content',
      'field_mededeling' => 'ds_content',
      'field_acp_order_status' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['commerce_order|commerce_order|default'] = $ds_layout;

  return $export;
}
