<?php

/**
 * @file
 * acp_inschrijvingen.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function acp_inschrijvingen_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_acp_akkoord|commerce_order|commerce_order|form';
  $field_group->group_name = 'group_acp_akkoord';
  $field_group->entity_type = 'commerce_order';
  $field_group->bundle = 'commerce_order';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'acp akkoord',
    'weight' => '2',
    'children' => array(
      0 => 'field_inschrijving_akkoord',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-acp-akkoord field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_acp_akkoord|commerce_order|commerce_order|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_acp|commerce_order|commerce_order|form';
  $field_group->group_name = 'group_acp';
  $field_group->entity_type = 'commerce_order';
  $field_group->bundle = 'commerce_order';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'acp',
    'weight' => '-1',
    'children' => array(
      0 => 'field_mededeling',
      1 => 'field_url_inschrijving',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-acp field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_acp|commerce_order|commerce_order|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('acp');
  t('acp akkoord');

  return $field_groups;
}
