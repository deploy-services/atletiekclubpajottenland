<?php

/**
 * @file
 * acp_inschrijvingen.features.inc
 */

/**
 * Implements hook_commerce_customer_profile_type_info().
 */
function acp_inschrijvingen_commerce_customer_profile_type_info() {
  $items = array(
    'billing' => array(
      'description' => 'The profile used to collect billing information on the checkout and order forms.',
      'help' => '',
      'addressfield' => TRUE,
      'module' => 'commerce_customer',
      'label_callback' => 'commerce_customer_profile_default_label',
      'type' => 'billing',
      'name' => 'Billing information',
    ),
  );
  return $items;
}

/**
 * Implements hook_commerce_product_default_types().
 */
function acp_inschrijvingen_commerce_product_default_types() {
  $items = array(
    'inschrijving' => array(
      'type' => 'inschrijving',
      'name' => 'Inschrijving',
      'description' => 'Gebruik om groep voor inschrijving atletiekjaar aan te maken',
      'help' => '',
      'revision' => 1,
    ),
    'product' => array(
      'type' => 'product',
      'name' => 'Product',
      'description' => 'A basic product type.',
      'help' => '',
      'revision' => 1,
    ),
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function acp_inschrijvingen_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function acp_inschrijvingen_node_info() {
  $items = array(
    'product_display' => array(
      'name' => t('Product Display'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
