<?php

/**
 * @file
 * acp_inschrijvingen.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function acp_inschrijvingen_default_rules_configuration() {
  $items = array();
  $items['commerce_payment_bank_transfer'] = entity_import('rules_config', '{ "commerce_payment_bank_transfer" : {
      "LABEL" : "Bank Transfer",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Payment" ],
      "REQUIRES" : [ "commerce_payment" ],
      "ON" : { "commerce_payment_methods" : [] },
      "DO" : [
        { "commerce_payment_enable_bank_transfer" : {
            "commerce_order" : [ "commerce-order" ],
            "payment_method" : "bank_transfer"
          }
        }
      ]
    }
  }');
  $items['rules_inchrijving_only_one_product_per_cart'] = entity_import('rules_config', '{ "rules_inchrijving_only_one_product_per_cart" : {
      "LABEL" : "Inchrijving_only_one_product_per_cart",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : { "commerce_cart_product_prepare" : [] },
      "IF" : [
        { "entity_is_of_type" : { "entity" : [ "commerce-product" ], "type" : "commerce_product" } },
        { "data_is" : { "data" : [ "commerce-product:type" ], "value" : "inschrijving" } }
      ],
      "DO" : [
        { "commerce_cart_empty" : { "commerce_order" : [ "commerce_order" ] } }
      ]
    }
  }');
  $items['rules_redirect_to_checkout'] = entity_import('rules_config', '{ "rules_redirect_to_checkout" : {
      "LABEL" : "redirect to checkout",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : { "commerce_cart_product_add" : [] },
      "DO" : [ { "redirect" : { "url" : "checkout" } } ]
    }
  }');
  return $items;
}
