(function ($) {
  Drupal.behaviors.acp_inschrijving_atletiekjaar = {
    attach: function (context, settings) {

      // Get birth selector.
      var birthYearElement = $('#edit-line-item-fields-field-inschrijving-geboortedatum-und-0-value-year');
      var atleetRecreantElement = $('input[type=radio][name=line_item_fields\\[field_atleet_of_recreant\\]\\[und\\]]');

      // Handle initial value.
      if (birthYearElement) {

        // Set correct product by birthyear.
        Drupal.behaviors.acp_inschrijving_atletiekjaar.setActiveProductByAge(birthYearElement, atleetRecreantElement, settings);


        // Handle change of birthyear switcher.
        birthYearElement.bind('change', function () {
          Drupal.behaviors.acp_inschrijving_atletiekjaar.setActiveProductByAge(birthYearElement, atleetRecreantElement, settings);
        });

        // Handle change atleet/recreant switcher.
        atleetRecreantElement.change(function () {
          Drupal.behaviors.acp_inschrijving_atletiekjaar.setActiveProductByAge(birthYearElement, atleetRecreantElement, settings);
        });
      }
    },
    setActiveProductByAge: function (birthYearElement, atleetRecreantElement, settings) {
      var birthyear = birthYearElement.val();
      var ageProductMapping = settings.age_product_mapping;

      // element where the selected product id is stored.
      // Change it according to age/atleet/recreant.
      var productIdElement = $('#edit-product-id');

      if (ageProductMapping && birthYearElement && productIdElement) {

        // Calculate current age
        var selectedAge = new Date().getFullYear() - birthyear;

        // Hide the product switcher element.
        productIdElement.hide();

        // Find product by chosen age.
        $.each(ageProductMapping, function (age, product) {
          if (selectedAge <= age) {
            var productId = product;

            // Option atleet/recreant available.
            if (typeof product === 'object') {
              $('#edit-line-item-fields-field-atleet-of-recreant').show();
              if (!document.getElementById('edit-line-item-fields-field-atleet-of-recreant-und-recreant').checked) {
                  $('#edit-line-item-fields-field-atleet-of-recreant-und-atleet').attr('checked', 'checked');
              }
              productId = product[atleetRecreantElement.filter(":checked").val()];

              // Hide default option.
              var defaultOptionElement = $("#edit-line-item-fields-field-atleet-of-recreant-und .form-type-radio").first();
              defaultOptionElement.hide();
            } else {
              $('#edit-line-item-fields-field-atleet-of-recreant').hide();
            }

            // set product id
            $('#edit-product-id').val(productId);

            return false;
          }
        });

      }
    }
  };

}(jQuery));