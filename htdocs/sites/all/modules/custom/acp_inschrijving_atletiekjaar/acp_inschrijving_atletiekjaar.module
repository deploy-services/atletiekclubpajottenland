<?php

/**
 * Implements hook_form_alter().
 */
function acp_inschrijving_atletiekjaar_form_alter(
    &$form,
    &$form_state,
    $form_id
)
{

    // Alter commerce add to cart form
    if (strpos($form_id, 'commerce_cart_add_to_cart_form') !== FALSE) {

        //only show selector for product type inschrijving
        if ($form_state['default_product']->type == 'inschrijving') {

            // Attach js when birth selector available.
            if (isset($form['line_item_fields'], $form['line_item_fields']['field_inschrijving_geboortedatum'])) {
                $age_product_mapping = acp_inschrijving_atletiekjaar_get_age_product_mapping();

                // Attach js
                $form['#attached']['js'] = [
                    drupal_get_path('module',
                        'acp_inschrijving_atletiekjaar') . '/acp_inschrijving_atletiekjaar.js',
                ];

                drupal_add_js(
                    [
                        'age_product_mapping' => $age_product_mapping,
                    ],
                    ['type' => 'setting']
                );
            }
        }
        else {
            $form['line_item_fields']['field_inschrijving_geboortedatum']['#access'] = FALSE;
            $form['line_item_fields']['field_atleet_of_recreant']['#access'] = FALSE;

        }
    }

    // Alter commerce bank transfer form
    if ($form_id == 'commerce_checkout_form_review') {

        $ogm = acp_inschrijving_atletiekjaar_generate_ogm($form_state['order']->order_id);

        global $base_url;
        $url_inschrijving = $base_url . "/inschrijving/" . $form_state['order']->uuid;


        if (isset($form['commerce_payment']['payment_details']['bank_details'])) {
            $form['commerce_payment']['payment_details']['bank_details']['#markup'] = t('Overschrijving met gestructureerde mededeling');
            $form['commerce_fieldgroup_pane__group_acp']['field_mededeling']['und'][0]['value']['#default_value'] = $ogm; //@TODO put in correct isset code
            $form['commerce_fieldgroup_pane__group_acp']['field_mededeling']['#access'] = FALSE;
            $form['commerce_fieldgroup_pane__group_acp']['field_url_inschrijving']['und'][0]['value']['#default_value'] = $url_inschrijving;
            $form['commerce_fieldgroup_pane__group_acp']['field_url_inschrijving']['#access'] = FALSE;

        }
    }
}

/**
 * Implements hook_menu().
 */
function acp_inschrijving_atletiekjaar_menu()
{
    return [
        'inschrijving/%' => [
            'title' => 'View Order',
            'page callback' => 'acp_inschrijving_atletiekjaar_order_view',
            'page arguments' => [1],
            'access callback' => 'acp_inschrijving_atletiekjaar_order_view_access',
            'type' => MENU_NORMAL_ITEM,
        ],

        // Route to payment matching form.
        'admin/commerce/orders/csv' => [
            'title' => 'Upload payments CSV',
            'description' => 'Match payments from CSV file.',
            'page callback' => 'drupal_get_form',
            'page arguments' => [
                'acp_inschrijving_atletiekjaar_csv_import_form',
            ],
            'access callback' => 'commerce_order_access',
            'access arguments' => ['create'],
            'weight' => 12,
            'file' => 'includes/acp_inschrijving_atletiekjaar.csv.inc',
        ],
    ];
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function acp_inschrijving_atletiekjaar_menu_local_tasks_alter(&$data, $router_item, $root_path)
{
    if ($root_path == 'admin/commerce/orders') {
        $item = menu_get_item('admin/commerce/orders/csv');
        if ($item['access']) {
            $data['actions']['output'][] = [
                '#theme' => 'menu_local_action',
                '#link' => $item,
            ];
        }
    }
}

function acp_inschrijving_atletiekjaar_order_view($uuid)
{

    try {

        $ids = entity_get_id_by_uuid('commerce_order', [$uuid]);
        $results = entity_load('commerce_order', $ids);


    } catch (Exception $exception) {
        return services_error($exception);
    }


    drupal_add_css(drupal_get_path('module',
            'commerce_order') . '/theme/commerce_order.theme.css');
    return entity_view('commerce_order', $results);


}

function acp_inschrijving_atletiekjaar_order_view_access()
{
    return TRUE;
}


/**
 * Implements hook_field_extra_fields().
 */
function acp_inschrijving_atletiekjaar_field_extra_fields()
{
    $extra['commerce_order']['commerce_order']['display']['field_acp_order_status'] = [
        'label' => t('ACP Order Status'),
        'description' => t('ACP order status extension'),
        'weight' => -10,
    ];
    return $extra;
}

function acp_inschrijving_atletiekjaar_entity_view(
    $entity,
    $type,
    $view_mode,
    $langcode
)
{

    if ($type == 'commerce_order' && $entity->type == 'commerce_order') {
        $extra_fields = field_extra_fields_get_display('commerce_order',
            $entity->type, $view_mode);

        if (!empty($extra_fields['field_acp_order_status']) && $extra_fields['field_acp_order_status']['visible'] == TRUE) {

            switch ($entity->status) {
                case "pending":
                    $div = '<div class = "acp-inschrijving-status-block-pending">';
                    $status_label = t('Betaling nog niet ontvangen');
                    break;
                case "completed":
                    $div = '<div class = "acp-inschrijving-status-block-completed">';
                    $status_label = t('Betaling ontvangen');
                    break;
            }

            $entity->content['field_acp_order_status'] = [
                '#markup' => $div . '<div class = "acp-field-label">' . t('Inschrijving Status') . '</div>' .
                    '<div class = "acp-inschrijving-status">' . $status_label. '</div></div>',
            ];
        }

        if (isset($entity->content['field_mededeling'])) {
            $ogm = $entity->content['field_mededeling'][0]['#markup'];
            $ogm = acp_inschrijving_atletiekjaar_format_ogm($ogm);
            $entity->content['field_mededeling'][0]['#markup'] = $ogm;
        }

    }
}


/**
 * Get mapping of related product per age.
 */
function acp_inschrijving_atletiekjaar_get_age_product_mapping()
{
    $age_product_mapping = [
        4 => 'MULTIMOVE',
        6 => 'KANGOEROE',
        8 => 'BENJAMINS',
        10 => 'PUPILLEN',
        12 => 'MINIEMEN',
        14 => 'CADETTEN',
        16 => 'SCHOLIEREN',
        18 => [
            'Atleet' => 'JUNIOR_ATLEET',
            'Recreant' => 'JUNIOR_RECREANT',
        ],
        35 => [
            'Atleet' => 'SENIOR_ATLEET',
            'Recreant' => 'SENIOR_RECREANT',
        ],
        99 => [
            'Atleet' => 'MASTER_ATLEET',
            'Recreant' => 'MASTER_RECREANT',
        ],
    ];

    asort($age_product_mapping);
    array_walk_recursive($age_product_mapping, 'acp_inschrijving_get_product_id_by_sku');

    return $age_product_mapping;
}

/**
 * The product switcher on the front-end uses the product entity ids,
 * replace the sku with the product id.
 *
 * @param $sku
 */
function acp_inschrijving_get_product_id_by_sku(&$sku)
{
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'commerce_product')
        ->entityCondition('bundle', 'inschrijving')
        ->propertyCondition('sku', $sku)
        ->addMetaData('account', user_load(1));

    $result = $query->execute();

    if (isset($result['commerce_product'])) {
        $sku = reset($result['commerce_product'])->product_id;
    }
}

/**
 * ACP generate OGM; 'gestructureerde mededeling'
 */
function acp_inschrijving_atletiekjaar_generate_ogm($order_id)
{

    $ogm = $order_id;
    $length = strlen($order_id);
    $length = 10 - $length;

    for ($i = 0; $i < $length; $i++) {
        $ogm = $ogm . rand(0,10);
    }

    $rest = $ogm % 97;

    if ($rest === 0) {
        $rest = 97;
    }
    if ($rest < 10) {
        $rest = "0" . $rest;
    }

    $ogm = $ogm . $rest;
    $ogm = substr($ogm, 0, 3) . "/" . substr($ogm, 3, 4) . "/" . substr($ogm, 7, 5);

    return $ogm;
}

function acp_inschrijving_atletiekjaar_format_ogm($ogm)
{

    $ogm_formatted = "+++" . $ogm . "+++";
    return $ogm_formatted;
}
