v <?php

/**
 * Payments csv import form.
 *
 * @param array $form
 * @param array $form_state
 *
 * @return array $form
 */
function acp_inschrijving_atletiekjaar_csv_import_form($form, &$form_state) {
  $form = [];

  $form['csv'] = [
    '#type'              => 'managed_file',
    '#title'             => t('CSV File'),
    '#description'       => '',
    '#upload_location'   => 'public://',
    '#upload_validators' => [
      'file_validate_extensions' => ['csv'],
    ],
    '#weight'            => 2,
  ];

  $form['submit'] = [
    '#type'   => 'submit',
    '#value'  => 'Match payments',
    '#weight' => 3,
  ];

  return $form;
}

function acp_inschrijving_atletiekjaar_csv_import_form_submit(
  $form,
  &$form_state
) {
  $file     = file_load($form_state['values']['csv']);
  $payments = acp_inschrijving_atletiekjaar_csv_parse_ogm_payments($file);

  if ($payments) {
    foreach ($payments as $payment) {
      // Update order with payment
      acp_inschrijving_atletiekjaar_mark_order_as_paid_by_OGM($payment);
    }
  }

  drupal_set_message('Orders updated with payment information');
}

/**
 * Generate list of all ogm payments.
 *
 * @param stdClass $file
 */
function acp_inschrijving_atletiekjaar_csv_parse_ogm_payments($file) {
  $payments = [];

  if (($handle = fopen($file->uri, "r")) !== FALSE) {
    $row = 1;
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

      // Skip first row.
      if ($row > 1 && $data[1] > 0) {
        // @todo check of mededeling het correcte formaat heeft alleen dan betaling toevoegen
        // https://www.php.net/manual/en/function.preg-match.php

        $payments[] = [
          'amount' => $data[1],
          'ogm'    => $data[6],
        ];
      }

      $row++;
    }
    fclose($handle);
  }

  return $payments;
}

/**
 * Check for order with OGM.
 *
 * @param stdClass $order
 */
function acp_inschrijving_atletiekjaar_mark_order_as_paid_by_OGM($payment) {
  // Get order for payment OGM.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'commerce_order')
        ->fieldCondition('field_mededeling', 'value', $payment['ogm'])
        ->propertyCondition('status', 'completed', '!=')
        ->addMetaData('account', user_load(1));

  $result = $query->execute();

  // In case an order matches the payment.
  if (isset($result['commerce_order'])) {
      $commerce_order_entity_ids = array_keys($result['commerce_order']);
      $commerce_order = entity_load_single('commerce_order', reset($commerce_order_entity_ids));
      $commerce_order_w = entity_metadata_wrapper('commerce_order', $commerce_order);

      if (commerce_payment_order_has_payments($commerce_order)) {
          if (!(  $commerce_order->order_id == '14' or
                  $commerce_order->order_id == '140' or
                  $commerce_order->order_id == '16' or
                  $commerce_order->order_id == '160' or
                  $commerce_order->order_id == '19' or
                  $commerce_order->order_id == '190' or
                  $commerce_order->order_id == '20' or
                  $commerce_order->order_id == '200' or
                  $commerce_order->order_id == '21' or
                  $commerce_order->order_id == '210' or
                  $commerce_order->order_id == '5' or
                  $commerce_order->order_id == '50' or
                  $commerce_order->order_id == '9' or
                  $commerce_order->order_id == '90' or
                  $commerce_order->order_id == '10' or
                  $commerce_order->order_id == '100'
          )) {
              $query = new EntityFieldQuery();

              $query
                  ->entityCondition('entity_type', 'commerce_payment_transaction')
                  ->propertyCondition('order_id', $commerce_order->order_id, '=')
                  ->propertyCondition('status', COMMERCE_PAYMENT_STATUS_FAILURE, '!=')
                  ->propertyCondition('payment_method', 'bank_transfer');

              $result = $query->execute();

              if (isset($result['commerce_payment_transaction'])) {
                  $commerce_pt_entity_ids = array_keys($result['commerce_payment_transaction']);
                  $commerce_pt_w = entity_metadata_wrapper('commerce_payment_transaction',
                      entity_load_single('commerce_payment_transaction', reset($commerce_pt_entity_ids)));

                  // Check if payment amount matches
                  $t_amount = $commerce_pt_w->amount->value() / 100;

                  if ($t_amount == $payment['amount']) {

                      // Set payment status to paid
                      $commerce_pt_w->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
                      $commerce_pt_w->save();

                      // Set order status to completed
                      $commerce_order_w->status = 'completed';
                      $commerce_order_w->save();
                  } else {
                      $commerce_pt_w->status = COMMERCE_PAYMENT_STATUS_FAILURE;
                      $commerce_pt_w->save();
                  }
              }
          }
      }
  }
}

